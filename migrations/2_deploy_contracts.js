var Token = artifacts.require("./Token.sol");
module.exports = function (deployer) {
    // 1.000.000 FXCE, 6 decimals
    deployer.deploy(Token, "FXCE Token", "FXCE", 1000000000, 6);
};
