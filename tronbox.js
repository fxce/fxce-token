module.exports = {
    networks: {
        mainnet: {
          privateKey: '',
          userFeePercentage: 0,
          feeLimit: 10e8,
          fullHost: 'https://api.trongrid.io',
          network_id: "*"
        },
        compilers: {
          solc: {
            version: '0.4.25'
          }
        }
    }
};
