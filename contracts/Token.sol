// 0.5.1-c8a2
// Enable optimization
pragma solidity ^0.4.25;

import "./Issuer.sol";
import "./TRC20Detailed.sol";

contract Token is TRC20Detailed {
    uint256 private issueAmount = 24000000;
    uint256 private _lastIssued;        // last issued in timestamp
    uint256 private _issuePeriod =  7603200; // 88 days (88 * 24 * 60 * 60)
    
    // list of address will be the folder of this token
    // run 'node address.js' to generate below data
    
    address[] private _holderAddress = [address(0x41da3a5d298820ecbb652be7d72aebcb5693ccbb7c), address(0x411f87b9e23ec157ecd2b798ddb8865fbce8553fc9), address(0x411bf06a4b7053b13cfa4e340a3917328b666ccd70), address(0x411b4d1d81f101e1f0e9c47731f11104c4866d8db6), address(0x416c82072626e293a0affb96e8395eb280b7fb65ff), address(0x413b2548e776fd3cc0f1c8203bb511b3d1953fc888), address(0x418934596b5018976bf9609bb63c8444dde514c811), address(0x4194f920d28ff67b1a28c5aaf8e10c9b8839392175), address(0x415d22daa140b184833ccd18d6653584b6137ebc58), address(0x41b85b17f18a494a4ae47ad94e318c47c232c7ff02), address(0x41dc88a75a095dd9ec110c32e85cbeddfa00a597d2), address(0x41004e264e479f11163d6fb26d07371cc453724a41)];

    uint256[] private _holderAmount = [200000000, 100000000, 50000000, 30000000, 140000000, 0, 0, 0, 0, 0, 0, 0];

    uint256[] private _holderLock = [1780246800, 1620838800, 1620838800, 1620838800, 1685552400, 1701363600, 1717174800, 1732986000, 1748710800, 1764522000, 1780246800, 1796058000];

    address[] private _issueAddress = [
      address(0x41d5d8b5c7696987998f8a55db2a329e2139cd92c6),
      address(0x4193c0a40b9e13f6a8eaa30147210a3d481b38ad24),
      address(0x410997efb804b3d615f09e46283d7fe98091a10df5)
    ];
    address private _issuerAddress = address(0x416224db4223c0ee21212bd695d3c044527608d581);
    
    /**
     * @dev Constructor that gives msg.sender and holders all of existing tokens.
     */
    constructor (string name, string symbol, uint supply, uint8 decimals) public TRC20Detailed(name, symbol, decimals) {
        for (uint i = 0; i < _holderAddress.length; i++) {
          _mint(_holderAddress[i], _holderAmount[i] * (10 ** uint256(decimals)));
          _lock(_holderAddress[i], _holderLock[i]);
          supply -= _holderAmount[i];
        }

        // mint init token to the issuer of contract
        _mint(_issuerAddress, supply * (10 ** uint256(decimals)));
        // add the issuer as a owner.
        _addIssuer(_issuerAddress);
    }

    /**
     * Transfer token to an address, only owner of contract.
     * @return true if transfer token success.
     */
    function issue(address to) public onlyIssuer returns (bool) {
        require(_lastIssued == uint256(0) || _lastIssued + _issuePeriod <= block.timestamp, "Issue failed: invalid period");
        require(to == _issueAddress[0] || to == _issueAddress[1], "Issue failed: invalid address");
        _transfer(msg.sender, to, issueAmount * (10 ** uint256(decimals())));
        _lastIssued = block.timestamp;
        return true;
    }

    /**
     * @dev Burns a specific amount of tokens.
     * @param value The amount of token to be burned.
     */
    function burn(uint256 value) public onlyMember returns (bool) {
        _burn(msg.sender, value);
        return true;
    }
}
