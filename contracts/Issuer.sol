pragma solidity ^0.4.25;

contract Issuer {
    mapping (address => bool) internal _issuers;

    constructor () internal {
    }

    modifier onlyIssuer() {
        require(isIssuer(msg.sender));
        _;
    }

    modifier onlyMember() {
        require(!isIssuer(msg.sender));
        _;
    }

    function isIssuer(address account) public view returns (bool) {
        return _issuers[account];
    }

    function _addIssuer(address account) internal {
        _issuers[account] = true;
    }
}
