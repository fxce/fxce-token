# fxce-token
Solidity smart contracts for the [TRON](https://tron.network) blockchain.

This repository contains a library of sample smart contracts for the TRON network.

Configured with [TronBox](https://github.com/tronprotocol/tron-box).

Contracts include but not limited to implementations of standard-compliant tokens, library kits, and common utilities.

## Usage

### Install tronbox:

```npm install -g tronbox```

### Compile:

```tronbox compile```

### Migrate:

```tronbox migrate --reset --network mainnet```

## License

Released under the [MIT License](LICENSE).
